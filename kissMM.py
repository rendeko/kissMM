# kissMM, a simple mod manager
# under GPLv3
from distutils.dir_util import copy_tree
import PySimpleGUI as sg
import configparser
config = configparser.ConfigParser()

programName = 'kissMM v2019-09-01'
newText = 'Idle'
config.read('config.cfg')
sg.ChangeLookAndFeel('Dark')
modList = config.get('MODS','data').split('\n')
baseGamePath = config.get('PATHS','BaseGamePath')
publishPath = config.get('PATHS','PublishPath')

def GetFolderName(string):
    splitList = string.split('\\')
    return splitList[-1]

def PrintBoth(string): # prints to both GUI and console
    window.Element('statusText').Update(string)
    print(string)

def PublishSingle(modPath):
    PrintBoth('Publishing ' + modPath)
    copy_tree(modPath, publishPath)

def PublishAll():
    PrintBoth('Starting publishing')
    for entry in modList:
        if ValidPath(entry):
            PublishSingle(entry)
    PrintBoth('Finished publishing')

def Republish():
    PrintBoth('Starting republishing')
    PrintBoth('Publishing ' + baseGamePath)
    copy_tree(baseGamePath, publishPath)
    PublishAll()

def ValidPath(entry): # checks for # and // comments
    if entry[0] != '#' and entry[0:2] != '//':
        return True

# PySimpleGUI
readableModList = []
pathModList = []
for entry in modList:
    if ValidPath(entry):
        readableModList.append(GetFolderName(entry))
        pathModList.append(entry)
buttonColumn = [sg.Button('Publish All')],[sg.Button('Republish All')]
layout = [[sg.Text('Mods:')],
    [sg.Listbox(values=(readableModList), bind_return_key=True, size=(60, 10),key='modListBox'), sg.Column(buttonColumn)],
    [sg.Text('', size=(60, 0), key='statusText')]]
window = sg.Window(programName, layout)

while True:      
    event, values = window.Read(timeout=0)
    if event is None:
        break
    elif event == 'Publish All':
        PublishAll()
    elif event == 'Republish All':
        Republish()
    elif event == 'modListBox': # force publish on double click. maybe add prompt?
        selectedEntry = values['modListBox'][0]
        for path in pathModList:
            if GetFolderName(path) == values['modListBox'][0]:
                PublishSingle(path)
                PrintBoth('Force published ' + path)
        window.Element('modListBox').Widget.curselection()
