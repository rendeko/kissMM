## kissMM
kissMM is a mod manager written in Python based around its sole design purpose - keep it simple, stupid. It acts as a GUI tool for duplicating your base game and copying mods into it.

-	Just a Python script that copies folders for you, really
-	Configured through a single file using folder paths
-	Runs on Windows and probably everything else
-	Under GPLv3

### Installation
You may need to install PySimpleGUI through `pip` if you don't have it already
1. Drag kissMM's script into its own folder
2. Create config.cfg in that folder (check the example for format)
3. Paste the folder locations for the mods you want to install into the config
4. Launch kissMM and hit **Republish All**, which will copy the base games files and your mods to your new game folder
5. Enjoy your game!

### Usage
-	**Publish All** copies all the mods in the list box
-	**Republish All** copies both the base game and all your mods
-	**Double click** a mod in the list box to publish it alone (useful for updates)
-	Mods are installed in the order they're listed in the config, from top (first) to bottom (last)
-	To disable a mod, just comment out the folder path in the config file

### Config example
Create the following config.cfg in the same directory as the kissMM script. Change the paths to what applies to your install.

```
[PATHS]
BaseGamePath = C:\Users\player\Games\MyGame\VanillaCopy
PublishPath = C:\Users\player\Games\MyGame\NewModifiedGame

[MODS]
data = C:\Users\player\Games\MyGame\Mods\Mod Folder A
	C:\Users\player\Games\MyGame\Mods\Mod Folder B
	C:\Users\player\Games\MyGame\Mods\Mod Folder C (etc etc...)
```

Paths can be commented out with a `#` or `//` prefix.

### To-do
-	CLI arguments to not need PySimpleGUI
-	Profiles
